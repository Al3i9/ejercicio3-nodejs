const Persona = require("./persona");

class Personas {

  constructor() {
    this._listado = [];
  }

  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const persona = this._listado[key];
      //console.log(persona);
      listado.push(persona);
    })
 
    return listado;
  }
 
  crearPersona(persona = {}) {
    // this._listado.push(persona);
    /// return this._listado;
    this._listado[persona.id] = persona;
  }

  cargarPersonasEnArray(personas=[]){
   //console.log(personas);
   personas.forEach(persona =>{
     this._listado[persona.id]=persona;
   })

   //console.log(this._listado);
  }

  borrarPersona(id ){
    console.log(this._listado);
    delete this._listado[id];
    console.log(this._listado);

  }

}

module.exports = Personas;