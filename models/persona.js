const { v4: uuidv4 } = require('uuid');

class Persona {
  
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;

  id_producto = {
    nombre : '',
  categoria : '',
  numero_serie : '',
  fecha_caducidad : '',
  }
  


  constructor(nombres, apellidos, ci, id_producto,nombre, categoria,numero_serie,fecha_caducidad ) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    
    
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Persona;